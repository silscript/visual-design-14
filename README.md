## Programming for Visual Designers
We will be learning front-end development today. Front-end is the practice of producing HTML, CSS, and JavaScript for a website or web application for a user to interact with it directly.

## Learning Objectives (5min)
- Differentiate between HTML and CSS.
- Explain how HTML is used for content.
- Explain how CSS is used for styling.
- Inspect an element using development tools.
- Know which content goes in the head.
- Know which content goes in the body.
- Learn the technical vocabulary.
- Link a stylesheet to HTML.
- Utilize common css properties.

## Web Development (30min)
Web development is the coding or programming that enables website functionality, per the owner's requirements. It mainly deals with the non-design aspect of building websites, which includes coding and writing markup. Web development ranges from creating plain text pages to complex web-based applications, social network applications and electronic business applications.

###### What can I build?
  - Websites
  - Web Applications
  - Native Applications (Mobile)

###### What are the differences between front-end vs back-end?
  - Front-end (HTML, CSS, JS) markup and programming languages are used to structure, style and make the user interface.
  - Back-end (Ruby, Python, etc.) programming langauges are used to process data on the server side and return information requested by the user interface.

###### What do programs actually do?
  - Keep track of values (variables).
  - Make decisions (conditionals).
  - Repeat things (loops).
  - Log things (log files).
  - Store things (databases).

###### What is version control?
  - Keeps track of code and versions.
  - Keeps track of contributions.
  - Collaborate with others on.
  - Open source, denoting software for which the original source code is made freely available and may be redistributed and modified.

## Website Preparation (10min)
In order to build and deploy a website or web application, you will need certain requirements. Today, we will be using Google Chrome as our browser and Sublime Text as our editor. If you are unable to install Sublime Text due to having a work computer or anything else, let us know! You may need to use the default text editor.

###### Requirements:
- A computer (Acer, Dell, Lenova, MacBook, Surface Pro, etc.).
- A web browser ([Chrome](https://www.google.com/chrome/browser/desktop/), [Edge](https://www.microsoft.com/en-us/download/details.aspx?id=48126), [FireFox](https://www.mozilla.org/en-US/firefox/new/), [Internet Explorer](http://windows.microsoft.com/en-us/internet-explorer/download-ie), [Safari](https://support.apple.com/downloads/safari), and [Opera](http://www.opera.com/)).
- A text editor ([Atom](https://atom.io/), Notepad, [Sublime Text](http://www.sublimetext.com/3), and TextEdit).

###### Deployment:
- A FTP client ([Cyberduck](https://cyberduck.io/), [FileZilla](https://filezilla-project.org/), [SmartFTP](https://www.smartftp.com/), and [Transmit](https://panic.com/transmit/)).
- A domain name (assigning addresses to internet web servers).
- A host (serves the pages for one or more websites).
- We will be using [BitBalloon](https://www.bitballoon.com/)! It's free!

## Deploy Your Website!
In order to deploy your website, please use this site.
- https://bitballoon.com/
- Sign up for a free account.
- Drag and drop your folder and your site is hosted!
- Show it off!
